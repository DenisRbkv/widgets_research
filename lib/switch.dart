import 'package:flutter/material.dart';

class CustomSwitch extends StatefulWidget {
  @override
  _CustomSwitchState createState() => _CustomSwitchState();
}

class _CustomSwitchState extends State<CustomSwitch> {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    return Switch(
      value: isSwitched,
      onChanged: (value){
        isSwitched=value;
        if (isSwitched) {
          print('true');
        } else {
          print('false');
        }
        setState(() {
        });
      },
      activeTrackColor: Colors.lightBlueAccent,
      activeColor: Colors.blueAccent,
    );
  }
}
