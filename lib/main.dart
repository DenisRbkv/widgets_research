import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:widget_research/date_time_picker_button.dart';
import 'switch.dart';
import 'slider.dart';

enum DrinkCharacter { beer, tea }

void main() => runApp(
      MaterialApp(
        debugShowCheckedModeBanner: false,
        home: MyHomePage(),
      ),
    );

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<MaterialColor> colorList = [
    Colors.lightBlue,
    Colors.yellow,
    Colors.green,      //lightBlue+yellow
    Colors.lightGreen, //green+yellow
    Colors.blue,       //lightBlue+green
    Colors.cyan,        //all
    Colors.grey,       //none
  ];

  bool lightBlueVal = true;
  bool yellowVal = false;
  bool greenVal = false;


  int i = 0;

  DrinkCharacter _character = DrinkCharacter.beer;

  TextEditingController _controller;

  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void changeColor() {
    if (lightBlueVal && yellowVal && greenVal) {
      i = 5;
    } else if (lightBlueVal && greenVal) {
      i = 4;
    } else if (greenVal && yellowVal) {
      i = 3;
    } else if (yellowVal && lightBlueVal) {
      i = 2;
    } else if (lightBlueVal) {
      i = 0;
    } else if (yellowVal) {
      i = 1;
    } else if (greenVal) {
      i = 2;
    } else {
      i = 6;
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('All widgets are working'),
          centerTitle: true,
          backgroundColor: colorList[i],
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 20.0),
              Padding(
                padding: const EdgeInsets.only(left: 17.0),
                child: CustomSwitch(),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  // [Monday] checkbox
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text("Light Blue"),
                      Checkbox(
                        value: lightBlueVal,
                        onChanged: (bool value) {
                          lightBlueVal = value;
                          changeColor();
                        },
                      ),
                    ],
                  ), // [Tuesday] checkbox
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text("Yellow"),
                      Checkbox(
                        value: yellowVal,
                        onChanged: (bool value) {
                          yellowVal = value;
                          changeColor();
                        },
                      ),
                    ],
                  ), // [Wednesday] checkbox
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Text("Green"),
                      Checkbox(
                        value: greenVal,
                        onChanged: (bool value) {
                          greenVal = value;
                          changeColor();
                        },
                      ),
                    ],
                  ),
                ],
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Text('$_character'),
              ),
              ListTile(
                title: const Text('Beer'),
                leading: Radio(
                  value: DrinkCharacter.beer,
                  groupValue: _character,
                  onChanged: (DrinkCharacter value) {
                    setState(() {
                      _character = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: const Text('Tea'),
                leading: Radio(
                  value: DrinkCharacter.tea,
                  groupValue: _character,
                  onChanged: (DrinkCharacter value) {
                    setState(() {
                      _character = value;
                    });
                  },
                ),
              ),
              Divider(),
              CustomSlider(),
              Divider(),
              Center(
                child: SizedBox(
                  width: 300,
                  child: TextField(
                    decoration: InputDecoration(
                        hintText: 'Write a text',
                    ),
                    controller: _controller,
                    onSubmitted: (String value) async {
                      await showDialog<void>(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: const Text('Thanks!'),
                            content: Text('You typed "$value".'),
                            actions: <Widget>[
                              FlatButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                child: const Text('OK'),
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      floatingActionButton: PickerButton(),
    );
  }
}
